## How to run a migration?

1. Update the `schema.zmodel`
2. Create and populate the `.env` file
3. Run `yarn zenstack generate --schema PATH_TO_ZENSTACK_SCHEMA`
4. Run `yarn prisma migrate dev --schema PATH_TO_PRISMA_SCHEMA`

## How to create your database based on Prisma schema?
`yarn prisma db push --schema PATH_TO_PRISMA_SCHEMA`